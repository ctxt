#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ctxt.h"

static void latex_doc_beg(struct doc *doc)
{
	doc_write(doc, "\\begin{document}\n");
}

static void latex_doc_end(struct doc *doc)
{
	doc_write(doc, "\\end{document}\n");
}

static void latex_head_beg(struct doc *doc, int level)
{
	switch (level) {
	case 0:
		doc_write(doc, "\\section*{");
		break;
	case 1:
		doc_write(doc, "\\subsection*{");
		break;
	default:
		doc_write(doc, "\\subsection*{");
	}
}

static void latex_head_end(struct doc *doc, int level)
{
	doc_write(doc, "}\n");
}

static void latex_par_beg(struct doc *doc)
{
}

static void latex_par_end(struct doc *doc)
{
}

static void latex_put(struct doc *doc, char *s)
{
	doc_write(doc, s);
}

static void latex_list_beg(struct doc *doc, int mark)
{
	doc_write(doc, "\\begin{itemize}\n");
}

static void latex_list_end(struct doc *doc)
{
	doc_write(doc, "\\end{itemize}\n");
}

static void latex_item_beg(struct doc *doc, char *head)
{
	doc_write(doc, "\\item ");
}

static void latex_item_end(struct doc *doc)
{
	doc_write(doc, "");
}

static void latex_block_beg(struct doc *doc, char *beg)
{
	doc_write(doc, beg ? beg : "\\begin{verbatim}");
	doc_write(doc, "\n");
}

static void latex_block_end(struct doc *doc, char *end)
{
	doc_write(doc, end ? end : "\\end{verbatim}");
	doc_write(doc, "\n");
}

static void latex_put_txt(struct doc *doc, char *s, char *m)
{
	char b[1024];
	switch(m[0]) {
	case '*':
		sprintf(b, "\\textbf{%s}", s);
		break;
	case '/':
		sprintf(b, "\\emph{%s}", s);
		break;
	case '%':
		strcpy(b, s);
		break;
	case '|':
		sprintf(b, "\\cite{%s}", s);
		break;
	case '[':
		sprintf(b, "\\footnote{%s}", s);
		break;
	default:
		sprintf(b, "%c%s%c", m[0], s, m[1]);
	}
	latex_put(doc, b);
}

static void latex_table_beg(struct doc *doc, int columns)
{
	int i;
	doc_write(doc, "\\begin{tabular}");
	if (columns) {
		doc_write(doc, "{|");
		for (i = 0; i < columns; i++)
			doc_write(doc, "l|");
		doc_write(doc, "}\n");
	}
}

static void latex_table_end(struct doc *doc)
{
	doc_write(doc, "\\hline\n");
	doc_write(doc, "\\end{tabular}\n");
}

/* a hack to identify the first entry in each row */
static int entcol;

static void latex_row_beg(struct doc *doc)
{
	entcol = 0;
	doc_write(doc, "\\hline\n");
}

static void latex_row_end(struct doc *doc)
{
	doc_write(doc, "\t\\\\\n");
}

static void latex_entry_beg(struct doc *doc)
{
	if (entcol++)
		doc_write(doc, "\t& ");
}

static void latex_entry_end(struct doc *doc)
{
}

struct fmt_ops latex_ops = {
	.doc_beg	= latex_doc_beg,
	.doc_end	= latex_doc_end,
	.head_beg	= latex_head_beg,
	.head_end	= latex_head_end,
	.par_beg	= latex_par_beg,
	.par_end	= latex_par_end,
	.list_beg	= latex_list_beg,
	.list_end	= latex_list_end,
	.item_beg	= latex_item_beg,
	.item_end	= latex_item_end,
	.table_beg	= latex_table_beg,
	.table_end	= latex_table_end,
	.row_beg	= latex_row_beg,
	.row_end	= latex_row_end,
	.entry_beg	= latex_entry_beg,
	.entry_end	= latex_entry_end,
	.block_beg	= latex_block_beg,
	.block_end	= latex_block_end,
	.put		= latex_put,
	.put_txt	= latex_put_txt
};
