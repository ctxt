#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "ctxt.h"

#define BUFFSIZE	2 * 1024

struct doc *doc_alloc(int fd)
{
	struct doc *doc = xmalloc(sizeof(struct doc));
	doc->fd = fd;
	doc->size = BUFFSIZE;
	doc->len = 0;
	doc->buf = xmalloc(BUFFSIZE);
	return doc;
}

static void flush_buf(int fd, char *buf, int len)
{
	int c = 0;
	int n = 0;
	while (len > n && (c = write(fd, buf + n, len - n)) >= 0)
		n += c;
}

static void doc_flush(struct doc *doc)
{
	flush_buf(doc->fd, doc->buf, doc->len);
	doc->len = 0;
}

void doc_memcat(struct doc *doc, char *s, int n)
{
	if (n > doc->size / 2) {
		doc_flush(doc);
		flush_buf(doc->fd, s, n);
		return;
	}
	if (n + doc->len > doc->size)
		doc_flush(doc);
	memcpy(doc->buf + doc->len, s, n);
	doc->len += n;
}

void doc_write(struct doc *doc, char *s)
{
	doc_memcat(doc, s, strlen(s));
}

void doc_free(struct doc *doc)
{
	doc_flush(doc);
	free(doc->buf);
	free(doc);
}
