#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ctxt.h"

static void html_doc_beg(struct doc *doc)
{
	doc_write(doc, "<body>\n");
}

static void html_doc_end(struct doc *doc)
{
	doc_write(doc, "\n</body>\n");
}

static int digits(int n)
{
	int d = 1;
	while ((n /= 10))
		d++;
	return d;
}

static char *putint(char *s, int n)
{
	int i;
	int d = digits(n);
	for (i = 0; i < d; i++) {
		s[d - i - 1] = n % 10 + '0';
		n /= 10;
	}
	s[d] = '\0';
	return s + d;
}

static char *putstr(char *dst, char *src)
{
	strcpy(dst, src);
	return strchr(dst, '\0');
}

static void html_head_beg(struct doc *doc, int level)
{
	char buf[64];
	char *s = buf;
	s = putstr(s, "<h");
	s = putint(s, level + 1);
	putstr(s, ">");
	doc_write(doc, buf);
}

static void html_head_end(struct doc *doc, int level)
{
	char buf[64];
	char *s = buf;
	s = putstr(s, "</h");
	s = putint(s, level + 1);
	putstr(s, ">");
	doc_write(doc, buf);
}

static void html_par_beg(struct doc *doc)
{
	doc_write(doc, "<p>\n");
}

static void html_par_end(struct doc *doc)
{
}

static void html_put(struct doc *doc, char *s)
{
	doc_write(doc, s);
}

static void html_list_beg(struct doc *doc, int mark)
{
	doc_write(doc, "<ul>\n");
}

static void html_list_end(struct doc *doc)
{
	doc_write(doc, "</ul>\n");
}

static void html_item_beg(struct doc *doc, char *head)
{
	doc_write(doc, "<li>");
}

static void html_item_end(struct doc *doc)
{
	doc_write(doc, "</li>\n");
}

static void html_block_beg(struct doc *doc, char *beg)
{
	doc_write(doc, beg ? beg : "<pre>");
	doc_write(doc, "\n");
}

static void html_block_end(struct doc *doc, char *end)
{
	doc_write(doc, end ? end : "</pre>");
	doc_write(doc, "\n");
}

static void html_put_link(struct doc *doc, char *d, char *s)
{
	char desc[1 << 10];
	char *link = desc;
	strcpy(desc, s);
	while ((link = strchr(link, ':'))) {
		if (link[1] == ' ')
			break;
		link++;
	}
	if (link)
		*link = '\0';
	sprintf(d, "<a href=\"%s\">%s</a>", link ? link + 2 : s, desc);
}

static void html_put_txt(struct doc *doc, char *s, char *m)
{
	char b[1024];
	switch (m[0]) {
	case '*':
		sprintf(b, "<b>%s</b>", s);
		break;
	case '/':
		sprintf(b, "<i>%s</i>", s);
		break;
	case '%':
		strcpy(b, s);
		break;
	case '|':
		html_put_link(doc, b, s);
		break;
	default:
		sprintf(b, "%c%s%c", m[0], s, m[1]);
	}
	html_put(doc, b);
}

static void html_table_beg(struct doc *doc, int columns)
{
	doc_write(doc, "<table>\n");
}

static void html_table_end(struct doc *doc)
{
	doc_write(doc, "</table>\n");
}

static void html_row_beg(struct doc *doc)
{
	doc_write(doc, "<tr>");
}

static void html_row_end(struct doc *doc)
{
	doc_write(doc, "</tr>\n");
}

static void html_entry_beg(struct doc *doc)
{
	doc_write(doc, "<td>");
}

static void html_entry_end(struct doc *doc)
{
	doc_write(doc, "</td>");
}

struct fmt_ops html_ops = {
	.doc_beg	= html_doc_beg,
	.doc_end	= html_doc_end,
	.head_beg	= html_head_beg,
	.head_end	= html_head_end,
	.par_beg	= html_par_beg,
	.par_end	= html_par_end,
	.list_beg	= html_list_beg,
	.list_end	= html_list_end,
	.item_beg	= html_item_beg,
	.item_end	= html_item_end,
	.table_beg	= html_table_beg,
	.table_end	= html_table_end,
	.row_beg	= html_row_beg,
	.row_end	= html_row_end,
	.entry_beg	= html_entry_beg,
	.entry_end	= html_entry_end,
	.block_beg	= html_block_beg,
	.block_end	= html_block_end,
	.put		= html_put,
	.put_txt	= html_put_txt
};
