/*
 * ctxt - a simple and fast plain text formatter
 *
 * Copyright (C) 2009-2012 Ali Gholami Rudi
 *
 * This program is released under the modified BSD license.
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "ctxt.h"

extern struct fmt_ops troff_ops;
extern struct fmt_ops latex_ops;
extern struct fmt_ops html_ops;

static struct fmt_ops *get_ops(char *name)
{
	if (!strcmp(name, "troff"))
		return &troff_ops;
	if (!strcmp(name, "latex"))
		return &latex_ops;
	if (!strcmp(name, "html"))
		return &html_ops;
	return NULL;
}

void die(char *msg)
{
	write(2, msg, strlen(msg));
	exit(1);
}

void *xmalloc(int size)
{
	void *value = malloc(size);
	if (!value)
		die("Out of memory!\n");
	return value;
}

static char *usage =
	"usage: ctxt [-m mode] [-e esc] <plaintext >formatted\n\n"
	"  mode: latex, html or troff (default: troff)\n";

int main(int argc, char **argv)
{
	struct txt *txt;
	struct doc *doc;
	char *mode = "troff";
	struct fmt_ops *ops;
	int esc = '\0';
	int i;
	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-' && argv[i][1] == 'm') {
			mode = argv[i][2] ? argv[i] + 2 : argv[++i];
			continue;
		}
		if (argv[i][0] == '-' && argv[i][1] == 'e') {
			esc = argv[i][2] ? argv[i][2] : argv[++i][0];
			continue;
		}
		die(usage);
	}
	if (!(ops = get_ops(mode)))
		die("unknown output format\n");
	txt = txt_alloc(0);
	doc = doc_alloc(1);
	format(doc, txt, ops, esc);
	doc_free(doc);
	txt_free(txt);
	return 0;
}
