struct doc {
	int fd;
	int len;
	int size;
	char *buf;
};

struct doc *doc_alloc(int fd);
void doc_write(struct doc *doc, char *s);
void doc_memcat(struct doc *doc, char *s, int n);
void doc_free(struct doc *doc);

struct fmt_ops {
	void (*doc_beg)(struct doc *doc);
	void (*doc_end)(struct doc *doc);
	void (*head_beg)(struct doc *doc, int level);
	void (*head_end)(struct doc *doc, int level);
	void (*par_beg)(struct doc *doc);
	void (*par_end)(struct doc *doc);
	void (*put)(struct doc *doc, char *s);
	void (*put_txt)(struct doc *doc, char *s, char *marker);
	void (*list_beg)(struct doc *doc, int mark);
	void (*list_end)(struct doc *doc);
	void (*item_beg)(struct doc *doc, char *mark);
	void (*item_end)(struct doc *doc);
	void (*table_beg)(struct doc *doc, int columns);
	void (*table_end)(struct doc *doc);
	void (*row_beg)(struct doc *doc);
	void (*row_end)(struct doc *doc);
	void (*entry_beg)(struct doc *doc);
	void (*entry_end)(struct doc *doc);
	void (*block_beg)(struct doc *doc, char *beg);
	void (*block_end)(struct doc *doc, char *end);
};

struct txt {
	char *txt;
	char **lines;
	int n;
};

struct txt *txt_alloc(int fd);
char *txt_line(struct txt *txt, int line);
void txt_free(struct txt *txt);

void format(struct doc *doc, struct txt *txt, struct fmt_ops *ops, int esc);

void die(char *msg);
void *xmalloc(int size);
