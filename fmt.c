#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ctxt.h"

#define MAXLINE		1024
#define LENGTH(vars)	(sizeof(vars) / sizeof(vars[0]))

static char *markers[] = {"**", "$$", "%%", "[]", "||", "//", "''", "``"};
struct block {
	char *beg;
	char *end;
	int txt;	/* process inline markups in the block */
} blocks[] = {
	{".#1", ".#2", 1}, {".!1", ".!2"},
	{".TS", ".TE", 1}, {".DS", ".DE", 1}, {".G1", ".G2"},
	{".EQ", ".EN"}, {".PS", ".PE"}, {".[", ".]"}, {".de", ".."},
};

struct fmt {
	struct doc *doc;
	struct txt *txt;
	struct fmt_ops *ops;
	int level;
	int esc;
};

static struct fmt *fmt_alloc(struct doc *doc, struct txt *txt,
			     struct fmt_ops *ops, int esc)
{
	struct fmt *fmt = xmalloc(sizeof(struct fmt));
	fmt->doc = doc;
	fmt->txt = txt;
	fmt->ops = ops;
	fmt->level = 0;
	fmt->esc = esc;
	return fmt;
}

static void fmt_free(struct fmt *fmt)
{
	free(fmt);
}

static char *fmt_line(struct fmt *fmt, int line)
{
	char *s = txt_line(fmt->txt, line);
	int i;
	for (i = 0; i < fmt->level && s && *s; i++)
		s++;
	return s;
}

static char *marker_char(char c, char p, char n)
{
	int i;
	if (p == '\\' || (p && isalnum(p)) || isspace(n))
		return NULL;
	for (i = 0; i < LENGTH(markers); i++)
		if (markers[i][0] == c)
			return markers[i];
	return NULL;
}

static char *fillbuf(char *beg, char *end)
{
	static char buf[MAXLINE];
	memcpy(buf, beg, end - beg);
	buf[end - beg] = '\0';
	return buf;
}

static void put_text(struct fmt *fmt, char *s)
{
	char *_s = s;
	char *o = s;
	while (*s) {
		char *r, *m;
		if (s[1] && s[0] == fmt->esc) {
			fmt->ops->put(fmt->doc, fillbuf(o, s));
			fmt->ops->put(fmt->doc, fillbuf(s + 1, s + 2));
			o = s + 2;
			s = s + 2;
			continue;
		}
		m = marker_char(*s, s == _s ? 0 : s[-1], s[1]);
		r = m ? strchr(s + 1, m[1]) : NULL;
		if (m && r) {
			fmt->ops->put(fmt->doc, fillbuf(o, s));
			fmt->ops->put_txt(fmt->doc, fillbuf(s + 1, r), m);
			o = r + 1;
			s = r + 1;
			continue;
		}
		s++;
	}
	fmt->ops->put(fmt->doc, o);
}

static void raw_line(struct fmt *fmt, char *s)
{
	fmt->ops->put(fmt->doc, s);
}

static void put_line(struct fmt *fmt, int n)
{
	put_text(fmt, fmt_line(fmt, n));
	put_text(fmt, "\n");
}

static void put_lines(struct fmt *fmt, int beg, int end)
{
	int i;
	for (i = beg; i < end; i++)
		put_line(fmt, i);
}

static void raw_lines(struct fmt *fmt, int beg, int end)
{
	int i;
	for (i = beg; i < end; i++) {
		raw_line(fmt, fmt_line(fmt, i));
		raw_line(fmt, "\n");
	}
}

static int indents(char *s)
{
	char *r = s;
	while (isspace(*r))
		r++;
	return r - s;
}

static int islist(char *first, char *line)
{
	char *signs = "*-+";
	if (!line) {
		if (!first || strlen(first) < 2)
			return 0;
		if (first[1] != ' ' || !strchr(signs, first[0]))
			return 0;
		return 1;
	}
	if (strlen(line) < 2)
		return 0;
	return line[0] == first[0] && line[1] == first[1];
}

static void fmt_handle(struct fmt *fmt, int beg, int end, int level);

static int fmt_head(struct fmt *fmt, int beg, int end)
{
	char *line = fmt_line(fmt, beg);
	char *next = fmt_line(fmt, beg + 1);
	char c;
	char *s = next;
	char *signs = "=-~";
	if (!next || !*line || beg == end || fmt->level)
		return 0;
	c = *next;
	if (!c || !strchr(signs, *next))
		return 0;
	while (*++s)
		if (*s != c)
			return 0;
	fmt->ops->head_beg(fmt->doc, strchr(signs, *next) - signs);
	put_text(fmt, line);
	fmt->ops->head_end(fmt->doc, strchr(signs, *next) - signs);
	return 2;
}

static int cmd_line(struct fmt *fmt, int i)
{
	return !fmt->level && *fmt_line(fmt, i) == '.';
}

static int par_end(struct fmt *fmt, int beg, int end)
{
	int i = beg;
	char *line;
	while (i < end && (line = fmt_line(fmt, i))) {
		if (!*line || indents(line) || islist(line, NULL) || cmd_line(fmt, i))
			break;
		i++;
	}
	return i;
}

static int fmt_par(struct fmt *fmt, int beg, int end)
{
	int i;
	if (indents(fmt_line(fmt, beg)) || cmd_line(fmt, beg))
		return 0;
	fmt->ops->par_beg(fmt->doc);
	i = par_end(fmt, beg, end);
	put_lines(fmt, beg, i);
	fmt->ops->par_end(fmt->doc);
	return i - beg;
}

static int fmt_rawline(struct fmt *fmt, int beg, int end)
{
	if (*fmt_line(fmt, beg) != '.')
		return 0;
	put_lines(fmt, beg, beg + 1);
	return 1;
}

static int min(int a, int b)
{
	return a < b ? a : b;
}

static int fmt_deindent(struct fmt *fmt, int n, int indent)
{
	char *line;
	int result = n;
	while ((line = fmt_line(fmt, n))) {
		if (*line && indents(line) < indent)
			break;
		n++;
		if (*line)
			result = n;
	}
	return result;
}

static int fmt_pre(struct fmt *fmt, int beg, int end)
{
	int level = indents(fmt_line(fmt, beg));
	int next = min(end, fmt_deindent(fmt, beg + 1, level));
	fmt->level += level;
	fmt->ops->block_beg(fmt->doc, NULL);
	raw_lines(fmt, beg, next);
	fmt->ops->block_end(fmt->doc, NULL);
	fmt->level -= level;
	return next - beg;
}

static int fmt_block(struct fmt *fmt, int beg, int end)
{
	struct block *blk = NULL;
	int next;
	char *line;
	int depth = 1;
	int i;
	line = fmt_line(fmt, beg);
	if (indents(line))
		return fmt_pre(fmt, beg, end);
	for (i = 0; i < LENGTH(blocks); i++)
		if (!strncmp(blocks[i].beg, line, strlen(blocks[i].beg)))
			blk = &blocks[i];
	if (!blk)
		return 0;
	next = beg + 1;
	while (next < end && depth > 0) {
		line = fmt_line(fmt, next++);
		if (!strncmp(blk->beg, line, strlen(blk->beg)))
			depth++;
		if (!strncmp(blk->end, line, strlen(blk->end)))
			depth--;
	}
	fmt->ops->block_beg(fmt->doc, fmt_line(fmt, beg));
	if (beg + 1 < next) {
		if (blk->txt)
			put_lines(fmt, beg + 1, next - 1);
		else
			raw_lines(fmt, beg + 1, next - 1);
	}
	fmt->ops->block_end(fmt->doc, fmt_line(fmt, next - 1));
	return next - beg;
}

static char *listhead(char *s, char *head)
{
	if (s[0] != '+')
		return NULL;
	s += 2;
	while (*s && *s != ':')
		*head++ = *s++;
	if (*s == ':')
		s++;
	while (isspace(*s))
		s++;
	*head = '\0';
	return s;
}

static int fmt_list(struct fmt *fmt, int beg, int end)
{
	int i = beg;
	char *first = fmt_line(fmt, i);
	char *line = first;
	if (!islist(first, NULL))
		return 0;
	fmt->ops->list_beg(fmt->doc, line[0]);
	while ((line = fmt_line(fmt, i)) && islist(first, line)) {
		int next = min(end, fmt_deindent(fmt, i + 1, 2));
		int head = i;
		char lhead[MAXLINE];
		if (line[0] == '+') {
			line = listhead(line, lhead);
			fmt->ops->item_beg(fmt->doc, lhead);
			put_text(fmt, line);
			put_text(fmt, "\n");
			head++;
		} else {
			fmt->ops->item_beg(fmt->doc, NULL);
		}

		fmt->level += 2;
		i = par_end(fmt, i, next);
		put_lines(fmt, head, i);
		fmt->level -= 2;

		fmt_handle(fmt, i, next, 2);
		i = next;
		fmt->ops->item_end(fmt->doc);
		if (i == end)
			break;
	}
	fmt->ops->list_end(fmt->doc);
	return i - beg;
}

/* .T1/.T2 tables */
static int fmt_table(struct fmt *fmt, int beg, int end)
{
	char *hdr = fmt_line(fmt, beg);
	int i;
	int row = 0, col = 0;
	if (hdr[0] != '.' || hdr[1] != 'T' || hdr[2] != '1')
		return 0;
	fmt->ops->table_beg(fmt->doc, 0);
	for (i = beg + 1; i < end; i++) {
		char *cur = fmt_line(fmt, i);
		if (cur[0] == '.' && cur[1] == 'T' && cur[2] == '2')
			break;
		if (cur[0] == '%') {
			raw_line(fmt, cur + 2);
			raw_line(fmt, "\n");
			continue;
		}
		if (strchr("_=\n", cur[0])) {
			if (col)
				fmt->ops->entry_end(fmt->doc);
			if (row)
				fmt->ops->row_end(fmt->doc);
			if (cur[0] != '\n') {
				raw_line(fmt, cur);
				raw_line(fmt, "\n");
			}
			col = 0;
			continue;
		}
		if (strchr("#*-+", cur[0])) {
			if (!col) {
				fmt->ops->row_beg(fmt->doc);
				row++;
			} else {
				fmt->ops->entry_end(fmt->doc);
			}
			fmt->ops->entry_beg(fmt->doc);
			col++;
		}
		fmt->level += 2;
		put_line(fmt, i);
		fmt->level -= 2;
	}
	if (col)
		fmt->ops->entry_end(fmt->doc);
	if (row)
		fmt->ops->row_end(fmt->doc);
	fmt->ops->table_end(fmt->doc);
	return i - beg + 1;
}

/* simple tables */

static int table_columns(char *line)
{
	int n;
	for (n = 0; *line; n++) {
		while (*line == '\t')
			line++;
		while (*line && *line != '\t')
			line++;
	}
	return n;
}

static void table_row(struct fmt *fmt, char *s)
{
	if (*s == '=' || *s == '-')
		return;
	fmt->ops->row_beg(fmt->doc);
	while (*s) {
		char *r = s;
		while (*s && *s != '\t')
			s++;
		fmt->ops->entry_beg(fmt->doc);
		put_text(fmt, fillbuf(r, s));
		fmt->ops->entry_end(fmt->doc);
		while (*s == '\t')
			s++;
	}
	fmt->ops->row_end(fmt->doc);
}

static int fmt_tableascii(struct fmt *fmt, int beg, int end)
{
	int i;
	int n;
	if (*fmt_line(fmt, beg) != '=')
		return 0;
	n = table_columns(fmt_line(fmt, beg + 1));
	fmt->ops->table_beg(fmt->doc, n);
	for (i = beg + 1; i < end; i++) {
		if (!fmt->level && !*fmt_line(fmt, i))
			break;
		table_row(fmt, fmt_line(fmt, i));
	}
	fmt->ops->table_end(fmt->doc);
	return i - beg;
}

/* parsing text */

static int (*parts[])(struct fmt *fmt, int beg, int end) = {
	fmt_head, fmt_list, fmt_table, fmt_tableascii,
	fmt_block, fmt_rawline
};

static void fmt_handle(struct fmt *fmt, int beg, int end, int level)
{
	int line = beg;
	int i;
	fmt->level += level;
	while (line < end) {
		int c = 0;
		if (!*fmt_line(fmt, line)) {
			put_line(fmt, line++);
			continue;
		}
		for (i = 0; i < LENGTH(parts); i++)
			if ((c = parts[i](fmt, line, end)))
				break;
		/* start a paragraph only after a blank line */
		if (!c && (line == beg || !*fmt_line(fmt, line - 1)))
			c = fmt_par(fmt, line, end);
		line += c;
		if (!c)
			put_line(fmt, line++);
	}
	fmt->level -= level;
}

void format(struct doc *doc, struct txt *txt, struct fmt_ops *ops, int esc)
{
	struct fmt *fmt = fmt_alloc(doc, txt, ops, esc);
	fmt->ops->doc_beg(doc);
	fmt_handle(fmt, 0, txt->n, 0);
	fmt->ops->doc_end(doc);
	fmt_free(fmt);
}
