#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "ctxt.h"

static char *file_read(int fd)
{
	int size = 1024;
	char *buf = xmalloc(size);
	int n = 0;
	int c = 0;
	while ((c = read(fd, buf + n, size - n)) > 0) {
		n += c;
		if (n == size) {
			char *newbuf = xmalloc(size * 2);
			memcpy(newbuf, buf, n);
			free(buf);
			buf = newbuf;
			size *= 2;
		}
	}
	return buf;
}

static int newlines(char *s)
{
	int i;
	for (i = 0; (s = strchr(s, '\n')); i++, s++);
	return i;
}

static void initlines(struct txt *txt)
{
	int i;
	txt->lines[0] = txt->txt;
	for (i = 1; i <= txt->n; i++) {
		char *s = strchr(txt->lines[i - 1], '\n');
		if (s)
			*s = '\0';
		if (i < txt->n)
			txt->lines[i] = s + 1;
	}
}

struct txt *txt_alloc(int fd)
{
	struct txt *txt = xmalloc(sizeof(struct txt));
	txt->txt = file_read(fd);
	txt->n = newlines(txt->txt);
	txt->lines = xmalloc(sizeof(*txt->lines) * txt->n);
	initlines(txt);
	return txt;
}

char *txt_line(struct txt *txt, int line)
{
	if (line < txt->n)
		return txt->lines[line];
	return NULL;
}

void txt_free(struct txt *txt)
{
	free(txt->lines);
	free(txt->txt);
}
