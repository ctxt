/*
 * The main challenge in troff backend is managing unnecessary spaces:
 * + no newline should appear in the output, except in blocks
 * + no excess whitespace should appear after inline dot commands
 *   like footnotes
 *
 * troff_put() manages white spaces:
 * + gotnl: is set when the last character is a newline
 * + eatspc: when one, ignore as much space as possible
 * + inblk: we are in a block and no whitespace processing should be done
 * + refer: right after .]] refer macro
 *
 * After printing an inline macro, eatspc_on() is called which forces
 * troff_put() to ignore all whitespaces until the first
 * non-space character.  Some care is necessary when handling dots; if
 * a dot appears just after a newline, it is a troff macro, otherwise
 * it is a printable dot and should be escaped.
 */
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ctxt.h"

static int eatspc;	/* jump space chars */
static int gotnl;	/* last output char was a newline */
static int inblk;	/* inside a block */
static int refer;	/* right after refer .]] */

static void eatspc_on(void)
{
	gotnl = 0;
	eatspc = 1;
}

static void troff_doc_beg(struct doc *doc)
{
}

static void troff_doc_end(struct doc *doc)
{
}

static void troff_put(struct doc *doc, char *s)
{
	if (!*s)
		return;
	if (inblk) {
		doc_write(doc, s);
		return;
	}
	if (refer) {
		char *r = s;
		while (*s && !isspace(*s))
			s++;
		doc_memcat(doc, r, s - r);
		doc_write(doc, "\n");
		refer = 0;
	}
	if (eatspc) {
		while (*s == ' ' || *s == '\t' || *s == '\n')
			gotnl = *s++ == '\n';
		eatspc = *s == '\0';	/* more eatspc if s was space */
		if (!gotnl && *s == '.')
			doc_write(doc, "\\&");
		else if (!eatspc)	/* exiting eatspc; set gotnl */
			gotnl = 1;
	}
	while (gotnl && *s == '\n')
		s++;
	while (*s) {
		char *r = strchr(s, '\n');
		r = r ? r + 1 : strchr(s, '\0');
		doc_memcat(doc, s, r - s);
		s = r;
		while (*s == '\n')
			s++;
	}
	gotnl = s[-1] == '\n';
}

static void troff_head_beg(struct doc *doc, int level)
{
	switch (level) {
	case 0:
		troff_put(doc, ".NH 1\n");
		break;
	case 1:
		troff_put(doc, ".NH 2\n");
		break;
	default:
		troff_put(doc, ".SH\n");
	}
}

static void troff_head_end(struct doc *doc, int level)
{
}

static void troff_par_beg(struct doc *doc)
{
	troff_put(doc, ".PP\n");
}

static void troff_par_end(struct doc *doc)
{
}

static int ldepth;

static void troff_list_beg(struct doc *doc, int mark)
{
	troff_put(doc, "\n.br\n");
	ldepth++;
	if (ldepth > 1)
		troff_put(doc, ".RS\n");
}

static void troff_list_end(struct doc *doc)
{
	if (ldepth > 1)
		troff_put(doc, ".RE\n");
	ldepth--;
}

static void troff_item_beg(struct doc *doc, char *head)
{
	char b[1024];
	if (head) {
		sprintf(b, ".IP \"%s\" 1i\n", head);
		troff_put(doc, b);
	} else {
		troff_put(doc, ".IP \\(bu 2\n");
	}
	eatspc_on();
}

static void troff_item_end(struct doc *doc)
{
}

static void troff_block_beg(struct doc *doc, char *beg)
{
	inblk = 1;
	troff_put(doc, beg ? beg : ".DS");
	troff_put(doc, "\n");
}

static void troff_block_end(struct doc *doc, char *end)
{
	troff_put(doc, end ? end : ".DE");
	troff_put(doc, "\n");
	inblk = 0;
}

static void troff_put_txt(struct doc *doc, char *s, char *m)
{
	char b[1024];
	switch(m[0]) {
	case '*':
		troff_put(doc, "\\f3");
		troff_put(doc, s);
		troff_put(doc, "\\fP");
		break;
	case '/':
		troff_put(doc, "\\f2");
		troff_put(doc, s);
		troff_put(doc, "\\fP");
		break;
	case '%':
		troff_put(doc, s);
		break;
	case '|':
		troff_put(doc, "\n.[[\n");
		troff_put(doc, s);
		troff_put(doc, "\n.]]");
		eatspc_on();
		refer = 1;
		break;
	case '[':
		troff_put(doc, "\n.FS\n");
		troff_put(doc, s);
		troff_put(doc, "\n.FE\n");
		eatspc_on();
		break;
	default:
		sprintf(b, "%c%s%c", m[0], s, m[1]);
		troff_put(doc, b);
	}
}

static void troff_table_beg(struct doc *doc, int columns)
{
	int i;
	troff_put(doc, ".TS\n");
	if (columns) {
		troff_put(doc, "allbox;\n");
		for (i = 0; i < columns; i++)
			troff_put(doc, "c ");
		troff_put(doc, ".\n");
	}
}

static void troff_table_end(struct doc *doc)
{
	troff_put(doc, ".TE\n");
}

/* a hack to identify the first entry in each row */
static int entcol;

static void troff_row_beg(struct doc *doc)
{
	entcol = 0;
}

static void troff_row_end(struct doc *doc)
{
	troff_put(doc, "\n");
}

static void troff_entry_beg(struct doc *doc)
{
	if (entcol++)
		troff_put(doc, "\t");
	troff_put(doc, "T{\n");
}

static void troff_entry_end(struct doc *doc)
{
	troff_put(doc, "\nT}");
}

struct fmt_ops troff_ops = {
	.doc_beg	= troff_doc_beg,
	.doc_end	= troff_doc_end,
	.head_beg	= troff_head_beg,
	.head_end	= troff_head_end,
	.par_beg	= troff_par_beg,
	.par_end	= troff_par_end,
	.list_beg	= troff_list_beg,
	.list_end	= troff_list_end,
	.item_beg	= troff_item_beg,
	.item_end	= troff_item_end,
	.table_beg	= troff_table_beg,
	.table_end	= troff_table_end,
	.row_beg	= troff_row_beg,
	.row_end	= troff_row_end,
	.entry_beg	= troff_entry_beg,
	.entry_end	= troff_entry_end,
	.block_beg	= troff_block_beg,
	.block_end	= troff_block_end,
	.put		= troff_put,
	.put_txt	= troff_put_txt
};
