CC = cc
CFLAGS = -Wall -O2
LDFLAGS =

all: ctxt
.c.o:
	$(CC) -c $(CFLAGS) $<
ctxt: ctxt.o txt.o doc.o fmt.o latex.o html.o troff.o
	$(CC) $(LDFLAGS) -o $@ $^
clean:
	rm -f *.o ctxt
